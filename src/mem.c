#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool block_is_big_enough(size_t query, struct block_header* block ) {
    return block->capacity.bytes >= query;
}
static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t round_pages(size_t mem) { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region (void const * addr, size_t query_size) {
  size_t region_size = region_actual_size(size_from_capacity((block_capacity) {query_size}).bytes);
  void *region_address = map_pages(addr, region_size, MAP_FIXED_NOREPLACE);
  if(region_address == MAP_FAILED){
      region_address = map_pages(addr, region_size, 0);
  }
  if(region_address == MAP_FAILED){
      return REGION_INVALID;
  }
  struct region reg = {
          .addr = region_address,
          .size = region_size,
          .extends = region_address == addr
  };
    block_init(region_address, (block_size){.bytes = region_size}, NULL);
    return reg;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return (struct block_header*) region.addr;
}

/*  освободить всю память, выделенную под кучу */
void heap_term() {
  struct block_header* curr_header = HEAP_START;
  while(curr_header != NULL){
      size_t curr_reg_size = 0;
      void* curr_reg_start = curr_header;
      while(curr_header -> next == block_after(curr_header)){
          curr_reg_size += size_from_capacity(curr_header -> capacity).bytes;
          curr_header = curr_header -> next;
      }
      curr_reg_size += size_from_capacity(curr_header -> capacity).bytes;
      curr_header = curr_header -> next;
      assert(munmap(curr_reg_start, curr_reg_size) != -1);
  }
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  query = size_max(query, BLOCK_MIN_CAPACITY);
  if(!block || !block_splittable(block, query)) return false;

  block_size block_size_first = size_from_capacity((block_capacity) {.bytes = query});
  block_size block_size_second = {size_from_capacity(block -> capacity).bytes - block_size_first.bytes};
  void *block2 = (void*) block + block_size_first.bytes;
  block_init(block2, block_size_second, block -> next);
  block_init(block, block_size_first, block2);
  return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block ) {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if (!block) return false;

  struct block_header *block_next = block -> next;
  if (!block_next || !mergeable(block, block_next)) {
      return false;
  }
  block -> capacity = (block_capacity) {.bytes = block -> capacity.bytes + size_from_capacity(block_next -> capacity).bytes};
  block -> next = block_next -> next;
  return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum { BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED } type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz ) {
    if (!block) {
        return (struct block_search_result) {.type = BSR_CORRUPTED };
    }

    for(;;) {
      while(try_merge_with_next(block));

      if (block -> is_free && block_is_big_enough(sz, block)){
          return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block = block};
      }
      if (block -> next == NULL){
          return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block = block};
      }
      block = block -> next;
  }
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  struct block_search_result result = find_good_or_last(block, query);
  if (result.type == BSR_REACHED_END_NOT_FOUND || result.type == BSR_CORRUPTED) {
      return result;
  }
  split_if_too_big(result.block, query);
  result.block -> is_free = false;
  return result;
}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if(last == NULL){
      return NULL;
  }
  void *region_next_addr = block_after(last);
  struct region region_next = alloc_region(region_next_addr, query);
  if(region_is_invalid(&region_next)) {
      return NULL;
  }
  last -> next = region_next.addr;
  if(region_next.extends && last -> is_free){
      try_merge_with_next(last);
      return last;
  }
  return region_next.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  if (!heap_start) return NULL;

  struct block_search_result result = try_memalloc_existing(query, heap_start);
  if(result.type == BSR_FOUND_GOOD_BLOCK){
      return result.block;
  }
  struct block_header *last_block_after = grow_heap(result.block, query);
  if(last_block_after == NULL){
      return NULL;
  }
  result = try_memalloc_existing(query, heap_start);
    return result.block;
}

void* _malloc( size_t query ) {
  if(!query) return NULL;

  struct block_header* const address = memalloc( query, (struct block_header*) HEAP_START );
  if (address) return address -> contents;
  return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while(try_merge_with_next(header));
}
