//
// Created by vaporplatinate on 12/17/23.
//
#include "mem.h"
#include "mem_internals.h"
#include "util.h"

#include <assert.h>
#include <stdlib.h>

void debug(const char * format, ...);

struct block_header *block_get_header(void* contents){
    return (struct block_header*) (((uint8_t*)contents) - offsetof(struct block_header, contents));
}

void test_init(void* arg){
    debug(arg);
    void *heap = heap_init(REGION_MIN_SIZE);
    assert(heap);
}
void test_malloc(){
    test_init("Test 1: heap init...");
    struct block_header *header = block_get_header(_malloc(228));
    assert(header -> capacity.bytes == 228);
    assert(header -> is_free == false);
    heap_term();
}

void test_free_single(){
    test_init("Test 2: single heap freeing...");

    void *p1 = _malloc(69);
    void *p2 = _malloc(69);
    void *p3 = _malloc(69);

    struct block_header *b1 = block_get_header(p1);
    struct block_header *b2 = block_get_header(p2);
    struct block_header *b3 = block_get_header(p3);

    assert(!b1 -> is_free);
    assert(!b2 -> is_free);
    assert(!b3 -> is_free);

    _free(b3);

    assert(!b1 -> is_free);
    assert(!b2 -> is_free);
    assert(b3 -> is_free);

    free(p1);
    free(p2);
    free(p3);

    heap_term();
}

void test_free_multiple(){
    test_init("Test 3: multiple heap freeing...");

    void *p1 = _malloc(420);
    void *p2 = _malloc(420);
    void *p3 = _malloc(420);

    struct block_header *b1 = block_get_header(p1);
    struct block_header *b2 = block_get_header(p2);
    struct block_header *b3 = block_get_header(p3);

    assert(!b1 -> is_free);
    assert(!b2 -> is_free);
    assert(!b3 -> is_free);

    _free(b1);

    assert(b1 -> is_free);
    assert(!b2 -> is_free);
    assert(!b3 -> is_free);

    _free(b2);

    assert(b2 -> is_free);
    assert(b2 -> capacity.bytes == offsetof(struct block_header, contents) + 420 * 2);
    assert(!b3 -> is_free);

    free(p1);
    free(p2);
    free(p3);

    heap_term();
}

void test_grow_heap(){
    test_init("Test 4: heap grow...");

    void *ptr = _malloc(REGION_MIN_SIZE);

    struct block_header *header = block_get_header(ptr);
    assert(header -> capacity.bytes == REGION_MIN_SIZE);
    assert(size_from_capacity(header -> capacity).bytes > REGION_MIN_SIZE);

    free(ptr);

    heap_term();
}

int main(){
    test_malloc();
    test_free_single();
    test_free_multiple();
    test_grow_heap();
    return 0;
}
